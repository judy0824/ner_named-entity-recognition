NLP Assignment 2 – Named Entity Recognition    
Description    
Named entities are phrases that contain the names of persons, organizations, locations, times and quantities.   
 
U.N. (organization) official Ekeus (person) heads for Baghdad (location)    
 
The task of CoNLL-2003 concerns named entity recognition. We will concentrate on four types of named entities: persons, locations, organizations and names of miscellaneous entities that do not belong to the previous three groups.     
The named entity tags have format B-TYPE, I-TYPE, O. The first word of the entity will have tag B-TYPE. I-TYPE means that the word is inside an entity of type TYPE. A word with tag O is not part of any entities. There are 9 entity tags in this task (B-LOC, B-PER, B-ORG, B-MISC, I-LOC, I-PER, I-ORG, I-MISC, O). Here is an example:     
 
AL-AIN B-LOC    
, O    
United B-LOC    
Arab I-LOC   
Emirates I-LOC    
1996-12-06 O    
 
For this assignment, you are offered training data, validation data and testing data.     
You use the training data for training deep learning model, the validation data for tuning hyper-parameters to improve performance of the model, the testing data for final evaluation.     
When the training process is done, you use the well-trained model to fill out the testing data with predicted named entity tags as submission.     
We will evaluate your submissions with ground truth. Tags are case-sensitive and all upper case. Be careful to process them.     
 
 
 
 
Requirements    
● Python programming language only     
● You can use any machine learning library TensorFlow, Keras, Pytorch, etc.     
● You can use snippets of code from open-source projects, tutorials, blogs, etc. Do not clone the entire projects directly. Try to implement a model by yourself.     
 
Grading     
We follow the definition of metrics introduced at CoNLL-2003 to measure the performance of the systems in terms of precision, recall and F1-score, where:      
 
“precision is the percentage of named entities found by the learning system that are correct. Recall is the percentage of named entities present in the corpus that are found by the system. A named entity is correct only if it is an exact match of the corresponding entity in the data file.”     
Models are evaluated based on exact-match F1-score on the testing data.    
● Completed Source Code (50%)    
● F1-Score (40%)    
● Report (10%)    
 
Submission Rule    
Please pack up your source code, test-submit.txt and report into a   
.zip file named as studentid_hw2.zip, and upload it to LMS. The following files must be included in your submission:    
● Python source code .py (one or more).    
● test-submit.txt: test-submit.txt with predicted entity tags    
● requirements.txt: installed libraries in your Python environment. To create a requirements file, use:    
pip freeze > requirements.txt   
● report.pdf: assignment report. You can write anything you want.   
 
Deadline: 2019/11/11   